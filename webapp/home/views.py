# webapp/home/views.py
from flask import render_template, Blueprint, url_for

home_blueprint = Blueprint(
    'home',
    __name__
)

@home_blueprint.route('/', methods=['GET','POST'])
def home_main():
    return render_template('home.html')